/**
 * Created By: Min Xu <xukmin@gmail.com>
 * Date: 07/08/2014

 * The program is to compute future investment value
 * using the following formula:
 * futureInvestmentValue = investmentAmount * 
 *                         (1 + monthlyInterestRate)^(numOfYears * 12)
 * 
 * The BigDecimal class is used for storing variables instead of double type 
 * and calculating the investment value
 * 
 * All the input values are checked if they are valid inputs.  
 */

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class InvestmentCalculator 
{
  public static void main(String[] args)
  {
    Scanner in = new Scanner(System.in);

    // Step 1: prompt the user to enter investment amount,
    //         check if it is a valid input
    System.out.print("Enter investment amount: ");

    boolean isBigDecimal = in.hasNextBigDecimal();
    BigDecimal investmentAmount = new BigDecimal(0);
    int isNegtive = -2; // for checking if it is negative

    if (isBigDecimal)
    {
      investmentAmount = in.nextBigDecimal();
      isNegtive = investmentAmount.compareTo(BigDecimal.ZERO);
    }

    while (!isBigDecimal || isNegtive == -1)
    {
      // Ignore the rest of the current input line.
      in.nextLine();

      System.out.println("Invalid input.");
      System.out.print("Re-enter investment amount: ");
      isBigDecimal = in.hasNextBigDecimal();

      if (!isBigDecimal) continue;

      investmentAmount = in.nextBigDecimal();
      isNegtive = investmentAmount.compareTo(BigDecimal.ZERO);
    }

    // Step 2: prompt the user to enter annual interest rate
    //
    System.out.print("Enter annual interest rate: ");
        
    while (!in.hasNextBigDecimal())
    {
      in.nextLine();
      System.out.println("Invalid input.");
      System.out.print("Re-enter annual interest rate: ");
    }
    BigDecimal annualInterestRate = in.nextBigDecimal();

    // Step 3: prompt the user to enter number of years
    System.out.print("Enter number of years: ");

    boolean isInt = in.hasNextInt();
    int numOfYears = 0;

    if (isInt)
    {
      numOfYears = in.nextInt();
    }

    while (!isInt || numOfYears < 0)
    {
      in.nextLine();
      System.out.println("Invalid input.");
      System.out.print("Re-enter number of years: ");
      isInt = in.hasNextInt();

      if (!isInt) continue;
      numOfYears = in.nextInt();
    }

    // Step 4: calculate the investment value
    //
    BigDecimal monthlyInterestRate =
        annualInterestRate.divide(new BigDecimal(12), 3, RoundingMode.HALF_UP);
    monthlyInterestRate = monthlyInterestRate.multiply(new BigDecimal(0.01));
    monthlyInterestRate = monthlyInterestRate.add(BigDecimal.ONE);
 
    BigDecimal value =
        investmentAmount.multiply(monthlyInterestRate.pow(numOfYears * 12)); 

    double  investment = value.doubleValue(); 
    System.out.printf("Accumulated value is %.2f%n", investment);
  }
}
