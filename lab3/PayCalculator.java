/** 
 *  Created By: Min Xu <xukmin@gmail.com>
 *  Date: 07/24/2014
 *
 *  This program is to calculate weekly pay for an employee,
 *  and then to print out the name of the employees and 
 *  their weekly pay amounts.
 *  
 *  The program performs validity check for each input.
 */

import java.util.*;

public class PayCalculator
{
  public static void main(String[] args)
  {
    Scanner in = new Scanner(System.in);

    boolean isContinue = true;
    ArrayList<Employee> list = new ArrayList<Employee>();

    // for the result output. It is true only if
    // there is one salaried employee added bonus.
    boolean hasBonus = false;

    while (isContinue)
    {
      // prompt users to input a name and a type
      String firstName = stringInputCheck(in, "First name of an employee:");
      String lastName = stringInputCheck(in, "Last name of an employee:");
      String name = firstName + " " + lastName;

      String promptType = "Select a type of the employee: \n" +
                  "1. Salaried\n2. Hourly\n3. Commission";
      int typeIndex = intInputCheck(in, promptType, 1, 3);

      String[] typePool = {"Salaried", "Hourly", "Commission"};
      String type = typePool[typeIndex - 1];

      if (type.equalsIgnoreCase("Salaried"))
      { 
        // for Salaried employee
        // prompt users to input weekly salary,
        // and whether adding 10% bonus
        double weeklySalary = doubleInputCheck(in, "Weekly Salary:"); 
        boolean isAddBonus = choiceInputCheck(in, "Adding 10% bonus[y/n]: ");

        if (isAddBonus)
        {
          hasBonus = true;
        }

        Employee e = new SalariedEmployee(name, type, weeklySalary, isAddBonus);
        list.add(e);
      } else if (type.equalsIgnoreCase("Hourly")) {
        // for Hourly employee
        // prompt users to input hours worked in the week,
        // and hourly rate.
        int hours = intInputCheck(in, "Hours worked in the week:",
                                  0, 100000);
        double hourlyRate = doubleInputCheck(in, "Hourly rate:");

        Employee e = new HourlyEmployee(name, type, hours, hourlyRate);
        list.add(e);  
      } else if (type.equalsIgnoreCase("Commission")) {
        // for Commission based employee
        // prompt users to input weekly base pay, 
        // and weekly sale
        double weeklyBasePay = doubleInputCheck(in, "Weekly base pay:");
        double weeklySale = doubleInputCheck(in, "Weekly sale:");

        Employee e = new CommissionBasedEmployee(name, type,
                                                 weeklyBasePay, weeklySale);
        list.add(e);
      }

      // prompt users to continue inputting or not 
      isContinue = choiceInputCheck(in, "Continue[y/n]: ");
    }

    System.out.println();
    System.out.printf("%-15s %-10s %5s %8s %10s %10s %10s\n",
                      "Name", "Status", "Hours", "Rate", 
                      "Weekly", "Sale", "Pay Amount");
    System.out.println("======================================" +
                       "======================================");
    
    for(int i = 0; i < list.size(); i++)
    {
      list.get(i).printDescription();
    }

    System.out.println("======================================" +
                       "======================================");
    if (hasBonus)
    {
      System.out.println("*A 10% bonus is awarded");
    }
    System.out.println();
  }

  public static String stringInputCheck(Scanner in, String prompt)
  {
    System.out.println(prompt);

    String line = in.nextLine();
    while (line.isEmpty())
    {
      System.out.println("Invalid input.");
      System.out.println("Re-enter " + prompt);
      line = in.nextLine();
    }
    return line;
  }

  public static double doubleInputCheck(Scanner in, String prompt)
  {
    System.out.println(prompt);

    while (!in.hasNextDouble())
    {
      in.nextLine();
      System.out.println("Invalid input.");
      System.out.println("Re-enter " + prompt);
    }
    double d = in.nextDouble();
    in.nextLine();
    return d;
  }

  public static int intInputCheck(Scanner in, String prompt,
                                  int low, int upper)
  {
    System.out.println(prompt);

    int index = 0;
    boolean isNext = in.hasNextInt();
    if (isNext)
    {
      index = in.nextInt();
    }

    while (!isNext|| index < low || index > upper )
    {
      in.nextLine();
      System.out.println("Invalid input.");
      System.out.println("Re-enter " + prompt);

      isNext = in.hasNextInt();
      if (!isNext) continue;
      index = in.nextInt();
    }

    in.nextLine();
    return index;
  }

  public static boolean choiceInputCheck(Scanner in, String prompt)
  {
    System.out.print(prompt);

    String choice = "";
    boolean isNext = in.hasNext();
    if (isNext)
    {
      choice = in.next();
    }
    while (!isNext ||
               (!choice.equalsIgnoreCase("y")
                  && !choice.equalsIgnoreCase("n")))
    {
      in.nextLine();
      System.out.println("Invalid input.");
      System.out.print(prompt);

      isNext = in.hasNext();
      if(!isNext) continue;

      choice = in.next();
    }

    in.nextLine();

    return choice.equalsIgnoreCase("y");
  }
}

/** The abstract class Employee is a base class.
 */
abstract class Employee
{
  public Employee(String name, String type)
  {
    this.name = name;
    this.type = type;
  }

  public String getName()
  {
    return name;
  }

  public String getType()
  {
    return type;
  }

  public abstract void printDescription();

  private String name;
  private String type;
} 

class SalariedEmployee extends Employee
{
  public SalariedEmployee(String name, String type, 
                          double weeklySalary, boolean isAddBonus)
  {
    super(name, type);
    this.weeklySalary = weeklySalary;
    this.isAddBonus = isAddBonus;
  }

  public double calculateWeeklyPay()
  {
    if (isAddBonus)
    {
      return weeklySalary * 40 * (1 + 0.1);
    } else {
      return weeklySalary * 40;
    }
  }

  public void printDescription()
  {
    System.out.printf("%-15s %-10s %5s %8s %10s %10s %10s",
                      getName(),
                      getType(),
                      "N/A",
                      "N/A",
                      String.format("$%.2f", weeklySalary),
                      "N/A",
                      String.format("$%.2f", calculateWeeklyPay()));

    if (isAddBonus)
    {
      System.out.println("*");
    } else {
      System.out.println();
    }
  }

  private double weeklySalary;
  private boolean isAddBonus;
}

class HourlyEmployee extends Employee
{
  public HourlyEmployee(String name, String type, 
                        int hours, double hourlyRate)
  {
    super(name, type);
    this.hours = hours;
    this.hourlyRate = hourlyRate;
  }

  public double calculateHourlyPay()
  {
    if (hours <= 40)
    {
      return hours * hourlyRate;
    } else {
      return 40 * hourlyRate + (hours - 40) * 2 * hourlyRate;
    }
  }

  public void printDescription()
  {
    System.out.printf("%-15s %-10s %5s %8s %10s %10s %10s\n",
                      getName(),
                      getType(),
                      String.format("%d", hours),
                      String.format("$%.2f", hourlyRate),
                      "N/A",
                      "N/A",
                      String.format("$%.2f", calculateHourlyPay()));
  }

  private int hours;
  private double hourlyRate;
}

class CommissionBasedEmployee extends Employee
{
  public CommissionBasedEmployee(String name, String type,
                                 double weeklyBasePay,
                                 double weeklySale)
  {
    super(name, type);
    this.weeklyBasePay = weeklyBasePay;
    this.weeklySale = weeklySale;
  }

  public double calculateCommissionPay()
  {
    return weeklyBasePay + 0.1 * weeklySale;
  }
 
  public void printDescription()
  {
    System.out.printf("%-15s %-10s %5s %8s %10s %10s %10s\n",
                      getName(),
                      getType(),
                      "N/A",
                      "N/A",
                      String.format("$%.2f", weeklyBasePay),
                      String.format("$%.2f", weeklySale),
                      String.format("$%.2f", calculateCommissionPay()));
  }

  private double weeklyBasePay;
  private double weeklySale;
}
