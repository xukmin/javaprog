/**
 * Created By: Min Xu <xukmin@gmail.com>
 * Date: 07/10/2014

 * The TicTacToeTest class is to design 5 test cases
 *  (including scenarios, input, expected result,and actual result)
 *  for testing the validity of the TicTacToe class.
 */

public class TicTacToeTest
{
  public static void main(String[] args)
  {
    int caseNumber = 1;
    TicTacToe[] game = new TicTacToe[5];

    for (int i = 0; i < 5; i++)
    {
      game[i] = new TicTacToe();
    }

    // Case 1: design for the winner being 'X'
    //
    // Before the game, the board should be
    //    1 2 3
    //
    //  A X| |X
    //    -----
    //  B O|O| 
    //    -----
    //  C  | |X
    String[] sBegin1 = {"A1", "B1", "A3", "B2", "C3"};
    String[] sPlay1 = {"A2", "C1"};

    System.out.println("Test case " + caseNumber + ":  ");
    System.out.println("After player 'X' filling in A2," +
                       "expected result should be player 'X' win.");

    playGame(sBegin1, sPlay1, game[0], caseNumber);
    caseNumber++;

    // Case 2: design for the winner being 'O'
    //
    // Before the game, the board should be
    //    1 2 3
    //
    //  A  | |O
    //    -----
    //  B X|O|X 
    //    -----
    //  C  | |X
    String[] sBegin2 = {"B1", "A3", "C3", "B2", "B3"};
    String[] sPlay2 = {"A1", "C1"};

    System.out.println("Test case " + caseNumber + ":  ");
    System.out.println("After player 'X' filling in A1, " +
                       "and player 'O' filling in C1," + 
                       "expected result should be player 'O' win.");

    playGame(sBegin2, sPlay2, game[1], caseNumber);
    caseNumber++;

    // Case 3: design for testing the user's input string
    //         which is not of the form "A1", "B2" etc.
    //
    // Before the game, the board should be
    //    1 2 3
    //
    //  A  |O|
    //    -----
    //  B  | | 
    //    -----
    //  C X| |
    String[] sBegin3 = {"C1", "A2"};
    String[] sPlay3 = {"abc", "A5", "D1"};

    System.out.println("Test case " + caseNumber + ":  ");
    System.out.println("Expected result should be " + 
                       "abc, A5 and D1 are invalid inputs.");

    playGame(sBegin3, sPlay3, game[2], caseNumber);
    caseNumber++;

    // Case 4: design for testing the user's input string
    //         which is of the form "A1", "B2" etc, 
    //         but the position has been taken by either player
    //
    // Before the game, the board should be
    //    1 2 3
    //
    //  A  |X|
    //    -----
    //  B  | | 
    //    -----
    //  C O| |
    String[] sBegin4 = {"A2", "C1"};
    String[] sPlay4 = {"C1", "A2"};

    System.out.println("Test case " + caseNumber + ":  ");
    System.out.println("Expected result should be " +
                       "C1 and A2 have been filled. " + 
                       "They are invalid inputs.");

    playGame(sBegin4, sPlay4, game[3], caseNumber);
    caseNumber++;

    // Case 5: design for the result being stalemate
    // 
    // Before the game, the board should be
    //    1 2 3
    //
    //  A X| |
    //    -----
    //  B O|X| 
    //    -----
    //  C  | |O
    String[] sBegin5 = {"A1", "B1", "B2", "C3"};
    String[] sPlay5 = {"C2", "A2", "C1", "A3", "B3"};

    System.out.println("Test case " + caseNumber + ":  ");
    System.out.println("After player 'X' filling in C2, C1 and B3, " +
                       "and player 'O' filling in A2 and A3, " + 
                       "expected result should be stalemate.");

    playGame(sBegin5, sPlay5, game[4], caseNumber);    
  }

  public static void fillBoardAndPrint(String[] anString, TicTacToe anGame)
  { 
    // begin at player 'X'
    if (anGame.getPlayer() == 'O')
    {
      anGame.switchTurn();
    }

    // loop all the elements in an inputting string
    for (String s : anString)
    {
      // check if it is an valid input
      if (!anGame.play(s))
      {
        System.out.println("Actual result is: " +
                           s + " is an invalid input.");
      }
 
      // check if it has won or stalemated
      if (anGame.won())
      {
        System.out.println("Actual result is: " + 
                           "Player " + anGame.getPlayer() + " Wins!");
      } else if (anGame.stalemate()){
        System.out.println("Actual result is: " +
                           "Stalemate!");
      }
      
      // switch the player
      anGame.switchTurn();
    }
   
    anGame.print();
    System.out.println();
  }

  public static void playGame(String[] initialS, String[] fillS, 
                              TicTacToe anGame, int caseNumber)
  {
    // initialize the board
    System.out.println("before the game:");
    fillBoardAndPrint(initialS, anGame);

    // play game
    fillBoardAndPrint(fillS, anGame);
  }
}
