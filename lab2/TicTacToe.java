/**
 * Created By: Min Xu <xukmin@gmail.com>
 * Date: 07/10/2014
 */

import java.util.Scanner;

public class TicTacToe
{
 /* 
  public static void main(String[] args) 
  {
    // test code
    TicTacToe ttt = new TicTacToe();
    ttt.print();

    Scanner in = new Scanner(System.in);
    final int TIMES = 3;

    for (int i = 0; i < TIMES; i++)
    {
      System.out.println("Please enter your choice:");
      String s = in.next();

      //boolean result = ttt.play(s);
      if (ttt.play(s))
      {
        ttt.print();
        ttt.switchTurn();
      }
    }
  } */

  private char[][] board;
  private char player; // 'X' or 'O'

  /* 
   * Instantiate board to be a 3 by 3 char array of spaces.
   * Set player to be 'X'.
   */
  public TicTacToe()
  {
    /*
     * Step 1: create an empty board, with an initial value
     * of a space (' ')
     */
    board = new char[3][3];

    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        board[i][j] = ' ';
      }
    }

    player = 'X';   
  }

  /* 
   * If s represents a valid move, 
   * add the current player's symbol to the board and return true.
   * Otherwise return false.
   */
  public boolean play(String s)
  {
    /* Step 2: Fill in here with your own
     * play logic, and replace the return with you
     * own.
     */

    // check whether string(s) has a correct form 
    if (s.length() != 2)
      return false;

    char firstC = s.charAt(0);
    char secondC = s.charAt(1);
    if (firstC != 'A' && firstC != 'B' && firstC != 'C')
      return false;
    if (secondC != '1' && secondC != '2' && secondC != '3')
      return false;

    // check whether the play is legal
    int row = 0;
    int col = 0;
    if (firstC == 'A')
    {
      row = 0;
    } else if (firstC == 'B') {
      row = 1;
    } else { // for firstC == 'c'
      row = 2;
    }

    if (secondC == '1')
    {
      col = 0;
    } else if (secondC == '2') {
      col = 1;
    } else { // for secondC == '3'
      col = 2;
    }

    if (board[row][col] != 'X' && board[row][col] != 'O')
    {
      board[row][col] = player;
      return true;
    }

    return false; 
  }

  /*
   * Switches the current player from X to O, or O to X.
   */
  public void switchTurn()
  {
    // Step 3: Fill in with your code to toggle between
    // 'X' and 'O'
    if (player == 'X')
    {
      player = 'O';
    } else { // for player == 'O'
      player = 'X';
    }
  }

  /*
   * Returns true if the current player has won the game.
   * Three in a row, column or either diagonal.
   * Otherwise, return false.
   */
  public boolean won()
  {
    /* Step 5: Fill in the code for the won method. This method
     * should return true if the current player has 3 in-a-row 
     * in any row, column or diagonal. Otherwise, return false.
     */
 
    int[] count = new int[8];
    for (int c : count)
    {
      c = 0;
    }
    
    for (int i = 0; i < 3; i++)
    {
      if (board[0][i] == player) count[0]++;
      if (board[1][i] == player) count[1]++;
      if (board[2][i] == player) count[2]++;

      if (board[i][0] == player) count[3]++;
      if (board[i][1] == player) count[4]++;
      if (board[i][2] == player) count[5]++;

      if (board[i][i] == player) count[6]++;
      if (board[i][2-i] == player) count[7]++;
    }

    for (int c : count)
    {
      if (c == 3)
      {
        return true;
      }
    }

    return false; // TODO: replace with your own return statement.
  }

  /*
   * Returns true if there are no places left to move
   */
  public boolean stalemate()
  {
    /*
     * Step 4: Fill in the code for the stalemate method. It
     * should return true if there are no more places to move 
     * on the board. Otherwise, return false return false; 
     */
    int count = 0;
    for (char[] row : board)
    {
      for (char col : row)
      {
        if (col == 'X' || col == 'O')
        {
          count++;
        }
      }
    }

    if (count == 9)
    {
      return true;
    } else {
      return false;
    }
  }

  public char getPlayer()
  {
    return player;
  }

  public void print()
  {
    System.out.println();
    System.out.println("\t  1 2 3");
    System.out.println();
    System.out.println("\tA "+board[0][0]+"|"+board[0][1]+"|"+board[0][2]);
    System.out.println("\t  -----");
    System.out.println("\tB "+board[1][0]+"|"+board[1][1]+"|"+board[1][2]);
    System.out.println("\t  "+"-----");
    System.out.println("\tC "+board[2][0]+"|"+board[2][1]+"|"+board[2][2]);
    System.out.println();

    // for test, will delete later
    //System.out.println("board[0][1] = a" + board[0][1] + "b");
   
  }

  /* 
   * Step 6: Main Method for Final Step - Delete your main method 
   * and uncomment this one. 
   * Runs the game by getting input from the user, making the 
   * appropriate moves, and prints who won or if it was a stalemate. 
   */ 

  public static void main(String[] args)
  {		
    Scanner in = new Scanner(System.in);
    TicTacToe game = new TicTacToe();
    System.out.println("Welcome to tic-tac-toe");
    System.out.println("Enter coordinates for your move following the X and O prompts");
		
    while(!game.stalemate())
    {
      //Print the game
      game.print();			
			
      //Prompt player for their move
      System.out.println("Please enter your choice:");
      String s = in.next();

      boolean result = game.play(s);
      //Loop while the method play does not return true when given their move.
      while (!result)
      {
        //Body of loop should ask for a different move
        System.out.println("Please re-enter your choice:");
        s = in.next();
        result = game.play(s);
      }

      //If the game is won, call break; 
      boolean isWon = game.won();
      if (isWon)
      {
        break;
      }	
						
      //Switch the turn
      game.switchTurn();
			
    } // end while loop for stalemate

    game.print();
    if(game.won())
    {
      System.out.println("Player "+game.getPlayer()+" Wins!!!!");
    } else {
      System.out.println("Stalemate");
    }
  } 

}
