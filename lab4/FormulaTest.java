/**
 * Created By: Min Xu <xukmin@gmail.com>
 * Date: 07/30/2014
 */

public class FormulaTest
{
  public static void main(String[] args)
  {
    Formula pf = new Formula(4.5, 10);
    String payment = pf.calculatePayment(1000);

    System.out.println("payment = " + payment);

    Formula pf2 = new Formula(5.2, 10);
    String loanAmount = pf2.calculateAmount(100);

    System.out.println("loanAmount = " + loanAmount);
  }
}
