// Created By: Min Xu <xukmin@gmail.com>
// Date: 07/30/2014

import java.math.*;
import java.text.*;

/** The class is to calculate monthly payment and loan amount
 *  based on the following formulae:
 *
 *  Monthly payment = (R * P) / (1 - (1 + R)^(-n))
 *  Loan amount = payment * (1 - (1 + R)^(-n)) / R
 *  where: 
 *    R = interest rate per period
 *    P = principal amount (loan amount)
 *    n = number of periodic payments
 */
public class Formula
{
  public Formula(double interestRate, int numberOfPeriod)
  {
    this.interestRate = interestRate * 0.01 / 12;
    this.numberOfPeriod = numberOfPeriod * 12;
  }

  public String calculatePayment(double principalAmount)
  { 
    setPrincipalAmount(principalAmount);

    double m = interestRate * principalAmount;
    double n = Math.pow((1 + interestRate), (-numberOfPeriod));

    payment = m / (1 - n);

    DecimalFormat df = new DecimalFormat("#.##");
    df.setRoundingMode(RoundingMode.HALF_EVEN);

    return df.format(payment);
  }

  public String calculateAmount(double payment)
  {
    setPayment(payment);
    
    double m = Math.pow((1 + interestRate), (-numberOfPeriod));
    principalAmount = payment * (1 - m) / interestRate;

    DecimalFormat df = new DecimalFormat("#.00");
    df.setRoundingMode(RoundingMode.HALF_EVEN);

    return df.format(principalAmount);
  }

  private void setPrincipalAmount(double principalAmount)
  {
    this.principalAmount = principalAmount;
  }

  private void setPayment(double payment)
  {
    this.payment = payment;
  }

  private double interestRate;
  private double principalAmount;
  private int numberOfPeriod;
  private double payment;
}
