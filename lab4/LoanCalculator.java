// Created By: Min Xu <xukmin@gmail.com>
// Date: 08/04/2014

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

/** This is a GUI program for a loan calculator using Swing,
 *  and the program is to calculate either the total loan amount
 *  or monthly payment based on user's selection.
 */

public class LoanCalculator
{
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
       {
         public void run()
         {
           LoanCalculatorFrame frame = 
               new LoanCalculatorFrame();
           frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           frame.setVisible(true);
         }
       });
  }
}

/**
 * A frame with a calculator panel
 */
class LoanCalculatorFrame extends JFrame
{
  public LoanCalculatorFrame()
  {
    setTitle("Loan Calculator");
    setLocationRelativeTo(null);

    CalculatorPanel panel = new CalculatorPanel();
    add(panel);
    pack();
  }
}

/**A panel for loan calculate:
 * with a button group for selection between monthly payment
 * and loan amount,
 * four text fields for input and output,
 * a button for calculating and a button for exit.
 *
 * When users click "exit" button, a dialog will be shown to 
 * prompt whether they would like to save the current setting.
 * 
 * Next time, the windows is initialized with the last sate.
 */
class CalculatorPanel extends JPanel
{
  public CalculatorPanel()
  {
    setLayout(new GridLayout(4,1));

    // Part 1: selection between monthly payment and loan amount
    JPanel topPanel = new JPanel();
    Border titled = BorderFactory.createTitledBorder("Calculate");
    topPanel.setBorder(titled);

    monthlyP = new JRadioButton("Monthly Payment", false);
    loanA = new JRadioButton("Loan Amount", false);
    ButtonGroup group = new ButtonGroup();
    group.add(monthlyP);
    group.add(loanA);

    ActionListener choice = new SelectionAction();
    monthlyP.addActionListener(choice);
    loanA.addActionListener(choice);

    topPanel.add(monthlyP);
    topPanel.add(loanA);
   
    // Part 2: four input fields
    textLoan = new JTextField();
    textRate = new JTextField();
    textYears = new JTextField();
    textPayment = new JTextField();

    JPanel panelInputA = new JPanel();
    panelInputA.setLayout(new GridLayout(2, 2));
    panelInputA.add(new JLabel("Loan Amount: ", SwingConstants.RIGHT));
    panelInputA.add(textLoan);
    panelInputA.add(new JLabel("Yearly interest Rate: ", SwingConstants.RIGHT));
    panelInputA.add(textRate);

    JPanel panelInputB = new JPanel();
    panelInputB.setLayout(new GridLayout(2, 2));
    panelInputB.add(new JLabel("Number of Years: ", SwingConstants.RIGHT));
    panelInputB.add(textYears);
    panelInputB.add(new JLabel("Monthly Payment: ", SwingConstants.RIGHT));
    panelInputB.add(textPayment);

    // Part 3: buttons of "calculate" and "exit"
    JPanel panelButton = new JPanel();
    panelButton.setLayout(new FlowLayout(FlowLayout.RIGHT));

    JButton buttonC = new JButton("Calculate");
    JButton buttonE = new JButton("Exit");

    ActionListener command = new CommandAction();
    buttonC.addActionListener(command);

    ActionListener close = new CloseAction();
    buttonE.addActionListener(close);

    panelButton.add(buttonC);
    panelButton.add(buttonE);
      
    add(topPanel);
    add(panelInputA);
    add(panelInputB);
    add(panelButton);

    // initialize:
    // read data of the previous state from the output file.
    // If the output file does not exist, the default setting 
    // is that the button of "monthly payment" is active.
    if (!readPreviousStateFromFile(fileName))
    {
      isPaymentActive = true;
      monthlyP.setSelected(true);
      switchTextField(textPayment, textLoan);
    }
  }

  // check the validity of input
  private boolean checkAndSetInputValues()
  {
    String sLoan = textLoan.getText();
    String sRate = textRate.getText();
    String sYears = textYears.getText();
    String sPay = textPayment.getText();

    // for the case: There is an empty input
    if (sRate.isEmpty() || sYears.isEmpty())
    {
      return false;
    }

    if (isPaymentActive && sLoan.isEmpty())
    {
      return false;
    }

    if (!isPaymentActive && sPay.isEmpty())
    {
      return false;
    }

    // for the case: an input string cannot be converted
    //               into a required type.
    try 
    {
      if (isPaymentActive)
      { 
        principalAmount = Double.valueOf(sLoan);
      } else {
        payment = Double.valueOf(sPay);
      }
      interestRate = Double.valueOf(sRate);
      numberOfPeriodic = Integer.valueOf(sYears);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  // inactivate t1, activate t2
  public void switchTextField(JTextField t1, JTextField t2)
  {
    t1.setEnabled(false);
    t1.setBackground(Color.LIGHT_GRAY);

    t2.setEnabled(true);
    t2.setBackground(Color.WHITE);
  }

  public void setTextField(JTextField t, String anText)
  {
    t.setDisabledTextColor(Color.BLACK);
    t.setText(anText);
  }

  public boolean readPreviousStateFromFile(String name)
  {
    File file = new File(name);
 
    if (!file.exists())
    {
      return false;
    }
 
    try 
    {
      Scanner scanner = new Scanner(file);

      String data = scanner.nextLine();
      //System.out.println(data);

      String[] m = data.split(",");

      textRate.setText(m[5]);
      textYears.setText(m[7]);
      setTextField(textLoan, m[3]);
      setTextField(textPayment, m[9]);
 
      isPaymentActive = Boolean.valueOf(m[1]);
      principalAmount = Double.valueOf(m[3]);
      interestRate = Double.valueOf(m[5]);
      numberOfPeriodic = Integer.valueOf(m[7]);
      payment = Double.valueOf(m[9]);

      if (isPaymentActive)
      {
        monthlyP.setSelected(true);
        switchTextField(textPayment, textLoan);
      } else {
        loanA.setSelected(true);
        switchTextField(textLoan, textPayment);
      }

      scanner.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  
    return true;
  }

  private class SelectionAction implements ActionListener
  {
    public void actionPerformed(ActionEvent event)
    {
      String selection = event.getActionCommand();
  
      if (selection.equalsIgnoreCase("monthly payment"))
      {
        isPaymentActive = true;
        switchTextField(textPayment, textLoan);
      } else {
        isPaymentActive = false;
        switchTextField(textLoan, textPayment);
      } 
    }
  }

  private class CommandAction implements ActionListener
  {
    public void actionPerformed(ActionEvent event)
    {
      String result = "";

      if (isPaymentActive)
      {
        if (!checkAndSetInputValues())
        {
          result = "invalid input";
        } else {
          Formula pf = new Formula(interestRate, numberOfPeriodic);
          result = pf.calculatePayment(principalAmount);
          payment = Double.valueOf(result);
          //System.out.println("payment = " + result);    
        }
     
        setTextField(textPayment, result);
      } else {
        if (!checkAndSetInputValues())
        {
          result = "invalid input";
        } else {
          Formula pf = new Formula(interestRate, numberOfPeriodic);
          result = pf.calculateAmount(payment);
          principalAmount = Double.valueOf(result);
          //System.out.println("Loan = " + result);
        }

        setTextField(textLoan, result);
      }
    }
  } 

  private class CloseAction implements ActionListener
  {
    public void actionPerformed(ActionEvent event)
    {
      if (dialog == null) //first time
      {
        dialog = new SaveDialog(SwingUtilities.getWindowAncestor(CalculatorPanel.this));
      }
      dialog.setVisible(true);
    }
  }

  class SaveDialog extends JDialog
  {
    public SaveDialog(Window owner)
    {
      super(owner, "Save Dialog", Dialog.ModalityType.APPLICATION_MODAL);

      String label = "Would you like to save the result in " +
                     fileName + "?";
      add(new JLabel(label), BorderLayout.CENTER);

      JButton yes = new JButton("Yes");
      JButton no = new JButton("No");

      ActionListener saveCommand = new SaveAction();

      JPanel panel = new JPanel();
      panel.add(yes);
      panel.add(no);
      yes.addActionListener(saveCommand);
      no.addActionListener(saveCommand);

      add(panel, BorderLayout.SOUTH);

      setSize(420, 120);
      setLocationRelativeTo(owner);

      this.owner = owner;
    }

    private class SaveAction implements ActionListener
    {
      public void actionPerformed(ActionEvent event)
      {
        String command = event.getActionCommand();

        // check whether the output file exists
        File file = new File(fileName);
        try
        {
          if (!file.exists())
          {
            file = File.createTempFile("saveCurrentState", "txt");
          }
        } catch (IOException e) {
          System.err.println("a file could not be created.");
        }

        if (command.equals("Yes"))
        {
          try
          {
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.print("isPaymentActive," + isPaymentActive + ",");
            writer.print("loanAmount," + principalAmount + ",");
            writer.print("yearlyInterestRate," + interestRate + ",");
            writer.print("numberOfYears," + numberOfPeriodic + ",");
            writer.print("monthlyPayment," + payment);
            writer.println();
            writer.close();
          } catch (FileNotFoundException e) {
            System.err.println("The file cannot be found.");
          } catch (UnsupportedEncodingException e) {
            System.err.println("the named charset is not supported.");
          }

        } else { // users click on "No" button
          file.delete();
        } 

        // terminate the program of loan calculator
        owner.dispose();      
      }
    }
    
    private Window owner;
  }

  private JRadioButton monthlyP;
  private JRadioButton loanA;

  private JTextField textLoan;
  private JTextField textRate;
  private JTextField textYears;
  private JTextField textPayment;

  private double interestRate;
  private double principalAmount; // loan amount
  private int numberOfPeriodic;
  private double payment;

  private boolean isPaymentActive;

  private SaveDialog dialog;
 
  private final String fileName = "saveCurrentState.txt";
}

